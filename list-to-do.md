List to do

Versi 0.03

1. Video Embed Prewedding
2. Fix Backsound
3. Fix Gallery

Versi 0.02

1. Pop-up buka undangan (DONE)
2. foto countdown (DONE)
3. pembatas do'a (DONE)
4. tanggal pernikahan, info akad nikah | resepsi, baru hotel mandiri, ingatkan (DONE)
5. galery ganti jadi slider (DONE)
6. comment section 5max, scroll more (bentuknya ada scroll dropdown) (DONE)

Versi 0.01

1. Section Galeri DONE
2. Acara Pernikahan -> Tombol kalender DONE
3. Turut Mengundang -> Orang-orang nya
4. Amplop Digital -> Background bank DONE
5. Ucapan & Doa -> Create Form, Show Ucapan by foreach
6. Slider -> Gambarnya dan button
